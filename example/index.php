<?php
require_once(dirname(__FILE__) . '/includes/init.php');

$sws = new SmartyWidgetSystem($smarty, dirname(__FILE__) . '/widgets');

$sws->addWidgetPosition(array('top', 'middle', 'bottom'));

$sws->loadWidget('first', array('test' => 'param', 'test2' => 'param2'), 'top');
$sws->loadWidget('first', array('test' => 'param'), 'middle');
$sws->loadWidget('twitter-feed', array('username' => 'big_ben_clock', 'height' => '500px', 'width' => '300px'), 'bottom');

$smarty->display(dirname(__FILE__) . '/templates/components/header.tpl');
$smarty->display(dirname(__FILE__) . '/templates/components/menu.tpl');
$smarty->display(dirname(__FILE__) . '/templates/index.tpl');
$smarty->display(dirname(__FILE__) . '/templates/components/footer.tpl');
?>