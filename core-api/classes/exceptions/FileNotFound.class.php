<?php

/**
 * FNFE is an exception that is thrown when an attempt to
 * load a file occurs and fails to find the file.
 *
 * @author Andrew Carlson
 */
class FileNotFoundException extends ErrorException {
    
    /** The file that failed to be found. */
    private $file_path = '';

    /**
     * Creates a FileNotFoundException for a file that was couldn't be found.
     * 
     * @param String $file_path The path of the file that failed to be found.
     * @param String $file The file that is throwing this exception(use __FILE__).
     * @param String $line_number The line number that this exceptino occured on(use __LINE__). 
     * @param ErrorException|null $previous_exception The previous exception that led to this one(if on exists).
     */
    public function __construct($file_path, $file, $line_number, $previous_exception) {
        parent::__construct('Couldn\'t find the file: ' + $file_path, 0, E_USER_ERROR, $file, $line_number, $previous_exception);
        $this->file_path = $file_path;
    }
    
    /**
     * Returnes the path the file that couldn't be found.
     * 
     * @return String The file.
     */
    public function getFilePath() {
        return $this->file_path;
    }
}

?>
