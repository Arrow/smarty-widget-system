<?php

require_once(dirname(__FILE__) . '/WidgetManifest.class.php');
require_once(dirname(__FILE__) . '/exceptions/FileNotFound.class.php');

/* Load the widget-api. */
require_once(dirname(__FILE__) . '/../../widget-api/load-widget-api.php');

/**
 * The main class of the Smarty Widget System, this class is a bridge
 * for your project into the SWS api. This file includes everything
 * required for SWS to run, no other core-api classes use includes.
 *
 * @author Andrew Carlson
 * @since 1.0
 * @version 1.0
 */
class SmartyWidgetSystem {
    
    /** The smarty instance for the project. */
    private $smarty = null;
    /** The path to the widgets directory. */
    private $widgets_directory = '';
    /** All registered widget positions. You must register a the position before adding a widget to it. */
    private $widget_positions = array();
    /** An array of the loaded widgets. */
    private $loaded_widgets = array();
    
    /**
     * Constructs the Smarty Widget System.
     * 
     * @param Smarty $smarty The Smarty instance.
     * @param String $widgets_directory The FULL PATH to the directory where widgets are stored.
     */
    public function __construct($smarty, $widgets_directory) {
        $this->smarty = $smarty;
        $this->widgets_directory = rtrim($widgets_directory, '/');
        
        require_once(dirname(__FILE__) . '/../smarty-plugins/register_widget_position.php');
        $smarty->registerPlugin('function', 'register_widget_position', 'register_widget_position');
        
        $smarty->assign('SmartyWidgetSystem', $this);
    }
    
    /**
     * Loads a new widget into this SWS instance.<br>
     * You MUST provide the absolute path!
     * 
     * @param String $widget_name The name of the widget, must be the same as it's folder name.
     * @param array $widget_params The parameters to pass to the widget's #setupWidget() method.
     * @param String $widget_position The position to add this widget to.(Note: The position must already exist.)
     * @throws FileNotFoundException If the widget's manifest or the widget's main class couldn't be found.
     */
    public function loadWidget($widget_name, $widget_params, $widget_position) {
        $continue = false;
        foreach($this->widget_positions as $positon) {
            if($positon == $widget_position) {
                $continue = true;
            }
        }
        if(!$continue) {
            return;
        }
        try {
            $widget_manifest = new WidgetManifest("{$this->widgets_directory}/$widget_name/manifest.xml");
            
            $widget_directory = $widget_manifest->getWidgetDirectory();
            $widget_class = "$widget_directory/$widget_name.class.php";
            
            if(!file_exists($widget_class)) {
                $widget_class = $widget_directory + "/$widget_name.php";
                if(!file_exists($widget_class)) {
                    throw new FileNotFoundException($widget_class, __FILE__, __LINE__, null);
                }
            }
            
            if(!class_exists($widget_name)) {
                include($widget_class);
            }
            $widget_class_name = str_replace('-', '', $widget_name); //TODO: Add class name defenition in manifest
            $widget = new $widget_class_name;
            $widget->setupWidget($widget_params); // TODO: Put this call in it's proper place
            $widget->setWidgetPosition($widget_position);
            array_push($this->loaded_widgets, $widget);
        } catch(FileNotFoundException $e)  {
            throw new FileNotFoundException($e->getFilePath(), __FILE__, __LINE__, $e);
        }
    }
    
    /**
     * Runs the #setupWidget() method on all of the loaded plugins.
     * @todo use this.
     * 
     */
    public function setupWidgets() {
        
    }
    
    /**
     * Creates a widget position where you can put new widgets.<br>
     * Names are forced to Strings
     * 
     * @param String|array $position_name The name of a new position or an A
     */
    public function addWidgetPosition($position_name) {
        if(is_array($position_name)) {
            foreach($position_name as $name) {
                array_push($this->widget_positions, (String)$name);
            }
        } else {
            array_push($this->widget_positions, (String)$position_name);
        }
    }
    
    /**
     * This returns every widget loaded using #loadWidget(String, String).<br>
     * You can use this if you want to see the list of loaded widgets for debugging.
     * 
     * @return array All the currently loaded widgets.
     */
    public function getLoadedWidgets() {
        return $this->loaded_widgets;
    }
}

?>
