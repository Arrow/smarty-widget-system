Smarty Widget System
====================

### Overview

This library is a simple way to implement a real widget system.
SWS is a universal widget system that works anywhere with PHP & Smarty.
It allows you to create and use widgets built for any site in your site that implements SWS.

### Requirements

What you need:

* **PHP5+**
* **Smarty 3.0+**

It's also recommended that you have:

* ** A MySQL Database **
* ** MySQLi PHP Extension **

In the future when SWS is more mature it will store widget info and have a sort of install/uninstall system, for this we recommend it work in a MySQL database and the code-api will use the MySQLi extension.
A filesystem storage system is planned as well, but it's still recommended you use MySQL.
