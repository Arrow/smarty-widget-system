<?php

/**
 * Description of first
 *
 * @author Andrew Carlson
 */
class first extends Widget {
    
    /** When my widget is setup, I put my output in here for safe keeping. */
    private $output = '';
    
    /**
     * Overriden from the Abstract Widget class.
     * Preps my widget at the same time as all of the other widgets.
     * 
     * @param array|null $params Parameters for this widget to use, provided by the user.
     */
    public function setupWidget($params) {
        $output = 'You gave me these parameters: <br>';
        if($params != null) {
            foreach($params as $key=>$param) {
                if($key == 'disable' && $param) {
                    /* You said don't run, so I wont. */
                    $this->can_run = false;
                    return;
                }
                $output .= "$key=$param<br>";
            }
            $output .= 'but I didn\'t want to use them.';
            $this->output = $output;
        }
    }
    
    /**
     * Smarty wants me, who am I to say no.
     * I'll just give it the $output variable I saved.
     * 
     * @return String My output.
     */
    public function getOutput() {
        return $this->output;
    }
}

?>
