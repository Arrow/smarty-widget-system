<?php
session_start();

//Include the Smarty library.
require(dirname(__FILE__) . '/../libraries/smarty/Smarty.class.php');

/* Setup smarty */
$smarty = new Smarty;
$smarty->force_compile = true;
$smarty->debugging = false;
$smarty->caching = false;
$smarty->cache_lifetime = 120;


//Include the SWS library, this is the only thing you need to include SWS handles the rest.
require(dirname(__FILE__) . '/../libraries/sws/core-api/classes/SmartyWidgetSystem.class.php');

?>