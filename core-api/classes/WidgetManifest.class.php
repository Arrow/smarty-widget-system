<?php

/**
 * An object form of the Widget Manifest file.
 * An instance of this class is created automatically
 * in SmartyWidgetSystem#load_widget
 *
 * @author Andrew Carlson
 */
class WidgetManifest {
    
    /** The widget author. */
    private $author = '';
    /** The name of the widget. */
    private $name = '';
    /** A description of the widget. */
    private $description = '';
    /** The widget's main class that extends Widget */
    private $main_class = '';
    /** The date the widget was created. */
    private $created_date = '';
    /** The date the widget was last updated. */
    private $updated_date = '';
    /** The version of the widget(no standard, just needs to be int/float). */
    private $version = 0;
    /** The widget's website. */
    private $website = '';
    /** The widget's support page. */
    private $support_website = '';
    /** THe directory of the widget(The folder housing this manifest). */
    private $widget_directory = '';
    
    public function __construct($manifest_file) {
        if(!file_exists($manifest_file)) {
            throw new FileNotFoundException($manifest_file, __FILE__, __LINE__, null);
        }
        
        $widget_manifest = new SimpleXMLElement(file_get_contents($manifest_file));
        
        $this->author = (String) $widget_manifest->author;
        $this->name = (String) $widget_manifest->name;
        $this->description = (String) $widget_manifest->description;
        $this->main_class = (String) $widget_manifest->main_class;
        $this->created_date = (String) $widget_manifest->created_date;
        $this->updated_date = (String) $widget_manifest->updated_date;
        $this->version = (Float) $widget_manifest->version;
        $this->website = (String) $widget_manifest->website;
        $this->support_website = (String) $widget_manifest->support_website;
        $this->widget_directory = dirname($manifest_file);
    }
    
    public function getAuthor() {
        return $this->author;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getDescription() {
        return $this->description;
    }
    
    public function getCreatedDate() {
        return $this->created_date;
    }
    
    public function getUpdatedDate() {
        return $this->updated_date;
    }
    
    public function getVersion() {
        return $this->version;
    }
    
    public function getWebsite() {
        return $this->website;
    }
    
    public function getSupportWebsite() {
        return $this->support_website;
    }
    
    public function getWidgetDirectory() {
        return $this->widget_directory;
    }
}
?>
