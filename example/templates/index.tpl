<div id="top">
    <h4>Top widget position</h4>
    {register_widget_position name="top" sws=$SmartyWidgetSystem}
</div>

<div id="middle">
    <h4>Middle widget position</h4>
    {register_widget_position name="middle" sws=$SmartyWidgetSystem}
</div>

<div id="bottom">
    <h4>Bottom widget position</h4>
    {register_widget_position name="bottom" sws=$SmartyWidgetSystem}
</div>