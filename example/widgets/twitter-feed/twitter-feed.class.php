<?php

/**
 * Adds a twitter feed to your website.
 *
 * @author Andrew Carlson
 */
class TwitterFeed extends Widget {
    
    private $output = '';
    
    public function setupWidget($params) {
        $username = $params['username'];
        $height = $params['height'];
        $width = $params['width'];
        
        $output = "
            <style type=\"text/css\">
                #twitter_update_list li {
                    border-style:solid;
                    border-width:1px;
                }
            </style>
            <div id=\"twitter_div\" style=\"font-size:14px;height:$height;width:$width;\">
                <h5>Twitter Feed Widget</h5>
                <ul id=\"twitter_update_list\"></ul>
                <script type=\"text/javascript\" src=\"http://twitter.com/javascripts/blogger.js\"></script>
                <script type=\"text/javascript\" src=\"https://api.twitter.com/1/statuses/user_timeline.json?screen_name=$username&include_rts=1&count=3&callback=twitterCallback2\"></script>
            </div>";
        
        $this->output = $output;
    }
    
    public function getOutput() {
        return $this->output;
    }
}
?>
