<?php

/**
 * Sets a widget position where all widgets registered
 * to $position_name will be displayed.<br>
 * @todo Remove the need to add the 'sws' parameter.
 * 
 * @param array $params The name of the position to set.
 * @param Smarty_Internal_Template $template The current template.
 */
function register_widget_position($params, $template) {
    if(!isset($params['name'])) {
        trigger_error('Widget Position failed to be set: Missing \'name\'.');
        return;
    } elseif(!isset($params['sws'])) {
        trigger_error('Widget Position failed to be set: Missing \'sws\'');
        return;
    }
    
    foreach($params['sws']->getLoadedWidgets() as $widget) { //TODO: Make it so we don't have to loop through every widget, every time
        if($widget->getWidgetPosition() == $params['name'] && $widget->can_run) {
            echo $widget->getOutput();
        }
    }
}
?>
