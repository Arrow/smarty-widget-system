<?php

/**
 * The base for SWS widgets.<br>
 * Do not use a constructor method to initilaize your widget's code,
 * put it in the setup_widget method so it will be called at the same
 * time as every other widget.
 *
 * @author Andrew Carlson
 * @since 1.0
 * @version 1.0
 */
abstract class Widget {
    
    /** Checked when Smarty is displaying the widget, set to false at anytim to stop this Widget. */
    public $can_run = true;
    /** The position this widget was assigned to, don't touch this, it's handled by SWS */
    private $position = '';
    
    /**
     * Called when a Widget is loaded.<br>
     * This isn't called when the object is loaded with SmartyWidgetSystem#load_widget(String),
     * it is called in a loop with all other widgets.<br>
     * This is only to prep the widget incase it is displayed, it doesn't mean it will be.<br>
     * It's recommended you surpress and handle errors yourself.<br>
     * If you have to stop your plugin from running, set $can_run to false.
     * 
     * @param array|null $params Parameters used for the widget, these can be formatted
     *                                  in anyway as it's up to the widget creator to parse them.
     */
    abstract public function setupWidget($params);
    /**
     * The content Smarty will output.
     * This is only called by Smarty when it's outputting the widget.
     */
    abstract public function getOutput();
    
    /**
     * Don't use this, it's used by SWS' internal api to set the position of a plugin.
     * 
     * @param String $position The position to assign the plugin to.
     */
    public function setWidgetPosition($position) {
        $this->position = $position;
    }
    
    /**
     * Get's the widget's position, used my SWS to figure
     * out where to display the widget.<br>
     * You can use this if you want but I don't see why you would need it,
     * maybe if you had a CMS with plugins that need to behave differntly
     * based on the position they have been set to.
     * 
     * @return String The widget's position.
     */
    public function getWidgetPosition() {
        return $this->position;
    }
}

?>
